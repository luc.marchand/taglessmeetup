# Tagless Meetup

Ce code est un code **non fonctionnel** qui m'a servi de support pour un talk lors d'un meetup organisé par Quantmetry.

Il s'agit ici d'une approche de code en Scala, en style OOP avec des Futures et en style FP avec [IO](https://typelevel.org/cats-effect/docs/2.x/datatypes/io) de Cats Effect.

Ce code est inspiré d'un code mission où nous avions fait deux ETL, un pour des KPIs internes et un autre pour des KPIs externes.

+ Le premier a été fait en suivant la logique OOP
+ Le dernier a été fait en suivant la logique FP.

Ce repo matérialise les enseignements que j'ai appris, sous forme de code.

Les slides vont sûrement arriver dans les prochains jours.