package fp.domain

case class Sale(createdAt: String, supplierId: Int, productId: String, totalSales: Double)
