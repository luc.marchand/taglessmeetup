package fp.usecase

trait UseCase[F[_], A]:
  def execute(): F[A]
  def name: String = getClass.getSimpleName

object UseCase:
  type RecordWritten = Int
