package fp.usecase

import cats.Monad
import fp.domain.{Order, Sale}
import fp.usecase.UseCase.*

class ImportSalesUseCase[F[_]: Monad](
    readOrders: F[List[Order]],
    writeSales: Seq[Sale] => F[RecordWritten]
) extends UseCase[F, RecordWritten]:
  import fp.infrastructure.InjectorImplicits.wsClient

  override def execute(): F[RecordWritten] =
    for
      orders <- readOrders
      filteredOrders = orders
        .filter(isAfterMEPDate)
        .filter(isNotFromTestEntity)
      sales = ordersToSales(filteredOrders)
      recordWritten <- writeSales(sales)
    yield recordWritten

object ImportSalesUseCase:
  /*
    Du code métier pour faire la transformation
  */
  
  def isAfterMEPDate(orders: List[Order]): Boolean = ???
  def isNotFromTestEntity(orders: List[Order]): Boolean = ???
  def ordersToSales(orders: List[Order]): List[Sale] = ???
