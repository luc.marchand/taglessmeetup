package fp.infrastructure

import cats.effect.IO
import fp.infrastructure.adapters.{ApiAdapter, MySqlAdapter}
import fp.infrastructure.repository.{OrdersRepository, SalesRepository}
import fp.usecase.ImportSalesUseCase

object InjectorImplicits:
  implicit val wsClient = ApiAdapter.getWSClient()
  implicit val mysqlConnection = MySqlAdapter.getMySqlConnection(MySqlAdapter.MySqlDatabase(...))


object Injectors:
  val importSalesUseCase: ImportSalesUseCase[IO] = new ImportSalesUseCase[IO](
    OrdersRepository.readRawOrders,
    SalesRepository.overwriteSales)
