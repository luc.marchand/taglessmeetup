package fp.infrastructure.repository

import cats.Foldable
import cats.effect.IO
import doobie.free.connection.ConnectionIO
import doobie.util.Write
import doobie.util.fragment.Fragment
import doobie.util.update.Update

trait MySQLTable[T] {
  import fp.infrastructure.InjectorImplicits._

  def tableName: String
  def columnNames: List[String]

  def insertMany(ps: List[T])(implicit W: Write[T], foldable: Foldable[List]): ConnectionIO[Int] = {
    val sqlInsert = s"INSERT INTO $tableName (${columnNames.mkString(",")}) VALUES (${columnNames.map(_ => "?").mkString(",")})"
    Update[T](sqlInsert).updateMany(ps)
  }

  def truncateTable(): ConnectionIO[Int] = {
    val query = s"TRUNCATE TABLE $tableName"
    Fragment(query, List.empty).update.run
  }

  def overwrite(rows: Seq[T])(implicit W: Write[T], foldable: Foldable[List]): IO[Int] = executeQuery (
    for {
      _     <- truncateTable()
      count <- insertMany(rows.toList)
    } yield count
  )(mysqlConnection)
}
