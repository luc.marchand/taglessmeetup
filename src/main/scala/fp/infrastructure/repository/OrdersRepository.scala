package fp.infrastructure.repository

import cats.effect.IO
import fp.domain.Order
import fp.infrastructure.InjectorImplicits
import fp.infrastructure.adapters.ApiAdapter

import scala.collection.Seq
import scala.concurrent.Future

object OrdersRepository:
  import InjectorImplicits.wsClient

  val LIMIT_PER_PAGE: Int = 100
  val ORDERS_API_ENDPOINT: String = "some-path"

  def readOrders: IO[Seq[JsValue]] =
    for
      authorizedWsClient   <- ApiAdapter.getAuthorizedWsClient(wsClient)
      ordersPageUrls       <- ApiAdapter.getPaginatedApiUrls(ORDERS_API_ENDPOINT, LIMIT_PER_PAGE)(authorizedWsClient)
      rawOrders: Seq[JsValue] <- ApiAdapter.fetchResourceFromPageUrlsParallel(ordersPageUrls, "Orders")(authorizedWsClient)
    yield castToOrder(rawOrders)
    
    
  def castToOrder(rawOrders: Seq[JsValue]): List[Order] = ???
