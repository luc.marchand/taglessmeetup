package fp.infrastructure.repository

import cats.effect.IO
import fp.domain.Sale

import scala.collection.Seq

object SalesRepository:
  def overwriteSales(sales: Seq[Sale]): IO[Int] = salesTable.overwrite(sales)
  
  def salesTable: MySQLTable[Sale] = new MySQLTable[Sale] {
    override def tableName: String = "sales"

    override def columnNames: List[String] = List("sale_date", "supplier_id", "product_id", "sale_in_currency")
  }

 
