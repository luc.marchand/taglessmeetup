package fp

import fp.infrastructure.Injectors

import scala.util.{Failure, Success, Try}

object MainFP extends App:
  
  object ExitCode:
    val SUCCESSFUL_EXIT_CODE = 0
    val FAILURE_EXIT_CODE = 1
  end ExitCode
  
  val jobList = List(Injectors.importSalesUseCase)
  
  def main(): Unit =
    val exitCodes: List[Int] = jobList
      .map(job => (job.name, Try(job.execute().unsafeRunSync())))
      .collect {
        case (jobName, Success(path)) => {
          println(s"$jobName has written successfully to $path")
          ExitCode.SUCCESSFUL_EXIT_CODE
        }
        case (jobName, Failure(exception)) => {
          println(s"$jobName failed")
          exception.printStackTrace()
          ExitCode.FAILURE_EXIT_CODE
        }
      }

    val exitCode = if (exitCodes.forall(_ == ExitCode.SUCCESSFUL_EXIT_CODE)) ExitCode.SUCCESSFUL_EXIT_CODE else ExitCode.FAILURE_EXIT_CODE
    System.exit(exitCode)
end MainFP
