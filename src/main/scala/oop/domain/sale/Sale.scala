package oop.domain.sale

case class Sale(createdAt: String, supplierId: Int, productId: String, totalSales: Double)
