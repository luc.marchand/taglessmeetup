package oop.domain.sale

import scala.concurrent.Future

trait SalesRepository:
  def writeSales(sales: Seq[Sale]): Future[Int]

