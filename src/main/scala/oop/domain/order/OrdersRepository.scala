package oop.domain.order

import scala.concurrent.Future

trait OrdersRepository:
  def getOrders(): Future[Seq[Order]]
