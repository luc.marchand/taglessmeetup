package oop.domain.order

import java.time.LocalDateTime

case class Order(createdAt: LocalDateTime, userId: Int, products: List[Product])

case class Product(productId: String, supplier: String, totalPrice: Double, quantity: Int)
