package oop

import oop.infrastructure.injector.ImportSalesUseCaseInjector
import oop.usecase.UseCase

object MainOOP extends App:
  lazy val jobs = List(ImportSalesUseCaseInjector.importSalesUseCase)

  def main(args: Array[String]): Unit = somethingToDoWith(jobs)
  
  def somethingToDoWith(jobs: List[Usecase[_]]) = ???
end MainOOP
