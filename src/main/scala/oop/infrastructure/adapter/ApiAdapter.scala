package oop.infrastructure.adapter

import fp.infrastructure.InjectorImplicits.wsClient

object ApiAdapter {
  type Client = String

  def getWSClient(): Client = ???

  def getAuthorizedWsClient(wsClient: Client) = ???

  def getPaginatedApiUrls(endpoint: String, limit: Int)(implicit client: Client) = ???

  def fetchResourceFromPageUrlsParallel(urls: List[String], entity: String)(implicit client: Client) = ???
}
