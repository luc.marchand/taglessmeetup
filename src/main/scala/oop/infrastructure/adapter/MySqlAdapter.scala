package oop.infrastructure.adapter

object MySqlAdapter {
  case class MySqlDatabase(name: String, username: String, password: String)

  def getMySqlConnection(database: MySqlDatabase, connectWithoutDatabase: Boolean = false) = ???
}
