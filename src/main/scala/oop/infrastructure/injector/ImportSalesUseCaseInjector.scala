package oop.infrastructure.injector

import oop.infrastructure.repository.{OrdersHTTPRepository, SalesMySqlRepository}
import oop.usecase.ImportSalesUseCase

object ImportSalesUseCaseInjector:
  val ordersHTTPRepository: OrdersHTTPRepository =
    new OrdersHTTPRepository("some-client")

  val salesMySqlRepository: SalesMySqlRepository = new SalesMySqlRepository("some-connexion")

  val importSalesUseCase: ImportSalesUseCase = new ImportSalesUseCase(ordersHTTPRepository, salesMySqlRepository)
