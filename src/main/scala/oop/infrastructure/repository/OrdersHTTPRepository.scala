package oop.infrastructure.repository

import oop.domain.order.{Order, OrdersRepository}
import oop.infrastructure.adapter.ApiAdapter

import scala.concurrent.Future

class OrdersHTTPRepository(wsClient: String) extends OrdersRepository:

  val LIMIT_PER_PAGE: Int = 100
  val ORDERS_API_ENDPOINT: String = "some-path"

  def readOrders: Future[List[Order]] =
    for {
      authorizedWsClient <- ApiAdapter.getAuthorizedWsClient(wsClient)
      ordersPageUrls <- ApiAdapter.getPaginatedApiUrls(ORDERS_API_ENDPOINT, LIMIT_PER_PAGE)(authorizedWsClient)
      rawOrders: Seq[JsValue] <- ApiAdapter.fetchResourceFromPageUrlsParallel(ordersPageUrls, "Orders")(authorizedWsClient)
    } yield castToOrder(rawOrders)


  def castToOrder(rawOrders: Seq[JsValue]): List[Order] = ???
