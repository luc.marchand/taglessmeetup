package oop.usecase

import oop.domain.order.*
import oop.domain.sale.*
import oop.domain.user.{UserInput, UserOutput, UsersInputRepository, UsersOutputRepository}

import scala.concurrent.Future

class ImportSalesUseCase(
  ordersRepository: OrdersRepository,
  salesRepository: SalesRepository
) extends UseCase[RecordWritten]:

  override def execute(): Future[RecordWritten] =
    for
      orders <- ordersRepository.getOrders()
      filteredOrders = orders
        .filter(isAfterMEPDate)
        .filter(isNotFromTestEntity)
      sales = ordersToSales(filteredOrders)
      recordWritten <- salesRepository.writeSales(sales)
    yield recordWritten

object ImportSalesUseCase:
  /*
    Du code métier pour faire la transformation
  */

  def isAfterMEPDate(orders: List[Order]): Boolean = ???
  def isNotFromTestEntity(orders: List[Order]): Boolean = ???
  def ordersToSales(orders: List[Order]): List[Sale] = ???
