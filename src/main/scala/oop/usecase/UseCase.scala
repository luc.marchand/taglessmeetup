package oop.usecase

import scala.concurrent.Future

trait UseCase[T]:
  def execute(): Future[T]
  def name: String = getClass.getSimpleName

object UseCase:
  type RecordWritten = Int