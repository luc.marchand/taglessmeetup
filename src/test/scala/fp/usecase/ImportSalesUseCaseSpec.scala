package fp.usecase

import cats.Id
import scala.collection.Seq

class ImportSalesUseCaseSpec extends AbstractSpec:

  "ImportSalesUseCase.execute" should {
    "return the number of lines written" in {

      // Given
      val orders = List[Order](...)
      val writeSales = (sales: Seq[Sale]) => sales.length
      val importSalesUseCase = new ImportSalesUseCase[Id](
        orders,
        writeSales
      )
      // When
      val result = importSalesUseCase.execute()
      // Then
      result should be(orders.length)

    }
  }

  /*
    autres tests des fonctions spécifiques au calcul des ventes
  */


