package oop.usecase

import oop.domain.order.{Order, OrdersRepository}
import oop.domain.sale.{Sale, SalesRepository}
import org.scalatest.matchers.should.Matchers.should

import java.util.*
import scala.concurrent.*
import scala.concurrent.duration.*

class ImportSalesUseCaseSpec extends AbstractSpec:

  "ImportSalesUseCase.execute" should {
    "write the right sales" in {
      val orders = List[Order](...)

      val ordersRepository = new OrdersRepository {
        override def getOrders(): Future[Seq[Order]] = Future.successful(orders)
      }

      val salesRepository = new SalesRepository {
        override def writeSales(sales: Seq[Sale]): Future[Int] = Future.successful(sales.length)

      }
      val importSalesUseCase = new ImportSalesUseCase(ordersRepository, salesRepository)

      val result = Await.result(importSalesUseCase.execute(), 3 minutes)

      result shouldBe orders.length
    }
  }
  /*
    autres tests des fonctions spécifiques au calcul des ventes
  */
